@extends('layout.default')
@section('content')
@if(count($errors) > 0)
<div class="alert alert-danger">
Inserting validation error<br><br>
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
    <h1 style="text-align:center"> ERROR: Not Authorized. </h1>

@endsection
