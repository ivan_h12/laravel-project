@extends('layout.default')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
			@if(Auth::check() && Auth::user()->role_id == 1)
				<a href="{{route('products.create')}}" class="btn btn-primary">Add Product</a> 
			@endif
			@foreach($products as $product)
				<div class="card">
					<div class="card-header">
						<b class="float-sm-left">{{ $product->name }}</b>
						@if(Auth::check() && Auth::user()->role_id == 1)
							<a href="/products/{{ $product->id }}" class="btn btn-primary float-sm-right">Edit Product</a> 
							<form id="delete-product" action="{{ route('product.destroy', $product->id) }}" method="POST">
							{{ csrf_field() }}
							@method('DELETE')
								<button class="btn btn-danger float-sm-right" type="submit" onclick="return confirm('Are you sure?')">Delete Product</button>
							</form>
						@endif
					</div>
					<div class="card-body">
						<ul>
							<p>Description: {{ $product->description }}</p>
							<p>Price: ${{ $product->price }}</p>
							<a href="{{route('products.edit', $product->id)}}" class="btn btn-primary float-sm-right">See Details</a> 
						</ul>
					</div>
				</div>
			@endforeach
        </div>
    </div>
</div>
@endsection
