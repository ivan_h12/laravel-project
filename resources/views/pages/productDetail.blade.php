@extends('layout.default')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Product</div>
                
                <div class="card-body">
                    <form method="post" action="{{route('products.update', $product->id)}}" enctype="multipart/form-data"> {!! method_field('put') !!} {{ csrf_field() }}
                        @csrf
                        <div class="form-group row">
                            <label for="nameProduct" class="col-md-4 col-form-label text-md-right">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nameProduct" value="{{$product->name}}" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="descriptionProduct" class="col-md-4 col-form-label text-md-right">Description</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="descriptionProduct" value="{{$product->description}}" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="priceProduct" class="col-md-4 col-form-label text-md-right">Price</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="priceProduct" value="{{$product->price}}" required readonly>
                            </div>
                        </div>

                        <div id="Buttons">
                            <button type="submit" class="btn btn-warning" id="black">Add to Cart</button>
                            <a href="{{URL::previous()}}" class="btn btn-outline-dark">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
