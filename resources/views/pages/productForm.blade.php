@extends('layout.default')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Product') }}</div>
                
                <div class="card-body">
                    <form method="POST" action="{{route('products.store')}}" enctype="multipart/form-data">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="nameProduct" class="col-md-4 col-form-label text-md-right">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nameProduct" placeholder="Name" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="descriptionProduct" class="col-md-4 col-form-label text-md-right">Description</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="descriptionProduct" placeholder="Description" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="priceProduct" class="col-md-4 col-form-label text-md-right">Price</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="priceProduct" placeholder="Price" required>
                            </div>
                        </div>

                        <div id="Buttons">
                            <button type="reset" class="btn btn-outline-info">Reset</button>
                            <button type="submit" class="btn btn-warning" id="black">Create</button>
                            <a href="{{URL::previous()}}" class="btn btn-outline-dark">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
