@extends('layout.default')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Profile</div>
                
                <div class="card-body">
                    <form method="post" action="{{route('users.update', $user->id)}}" enctype="multipart/form-data"> {!! method_field('put') !!} {{ csrf_field() }}
                        @csrf
                        <div class="form-group row">
                            <label for="emailUser" class="col-md-4 col-form-label text-md-right">E-mail</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="emailUser" value="{{$user->email}}" readonly="true" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nameUser" class="col-md-4 col-form-label text-md-right">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nameUser" value="{{$user->name}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="zipCodeUser" class="col-md-4 col-form-label text-md-right">Zip Code</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="zipCodeUser" value="{{$user->zip_code}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="addressUser" class="col-md-4 col-form-label text-md-right">Address</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="addressUser" value="{{$user->address}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phoneNumberUser" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="phoneNumberUser" value="{{$user->phone_number}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="img" class="col-md-4 col-form-label text-md-right">{{ __('Image') }}</label>
                            <div class="col-md-6">
                                <img class="img-rounded corners" width="60px" src="{{ asset('images/user/'.$user->image) }}" alt="">
                            </div>
                            
                        </div>
                        <div class="form-group row">
                            <label for="userImage" class="col-md-4 col-form-label text-md-right"></label>
                            <div class="col-md-6">
                                <input type="file" class="form-control-file" name="userImage" accept="image/*">
                            </div>
                        </div>

                        <div id="Buttons">
                            <button type="reset" class="btn btn-outline-info">Reset</button>
                            <button type="submit" class="btn btn-warning" id="black">Edit</button>
                            <a href="{{URL::previous()}}" class="btn btn-outline-dark">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
