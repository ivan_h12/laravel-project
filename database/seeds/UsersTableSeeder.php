<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facade\Hash;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        
        $adminRole = Role::where('name', 'admin')->first();
        $userRole = Role::where('name', 'user')->first();

        $admin = User::create([
            'name' => 'Admin User',
            'email' => 'admin@webshop.nl',
            'password' => password_hash('adminadmin', PASSWORD_BCRYPT),
            'address' => 'Rachelsmolen 1',
            'phone_number' => '0123456789',
            'zip_code' => '5612MA',
            'role_id' => '1'
        ]);

        $user = User::create([
            'name' => 'User User',
            'email' => 'user@webshop.nl',
            'password' => password_hash('useruser', PASSWORD_BCRYPT),
            'address' => 'Kruistraat 1',
            'phone_number' => '0987654321',
            'zip_code' => '5502JA',
            'role_id' => '2'
        ]);

        $admin->roles()->attach($adminRole);
        $user->roles()->attach($userRole);
    }
}
