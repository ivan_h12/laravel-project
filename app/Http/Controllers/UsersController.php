<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use File;
use Image;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "Get All User";
    }

    public function getUserProfile($id)
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentUser = Auth::user();
        //dd($currentUser);
        $user = User::findOrFail($id);
        return view('pages.profile', compact('user'));

        if($currentuser->can('view', $user)) {
            $user = User::findOrFail($id);
           
        } else {
            return view('pages.notallowed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        if(Auth::check() && Auth::user()->id == $user->id) {
            $user->email = $request->emailUser;
            $user->name = $request->nameUser;
            $user->address = $request->addressUser;
            $user->zip_code = $request->zipCodeUser;
            $user->phone_number = $request->phoneNumberUser;

            if ($request->hasFile('userImage')) {
                //check if user already has an image
                if($user->image != 'noImage.jpg'){
                    //delete old user image
                    $image_path = public_path("images/user/" . $user->image);
                    if(File::exists($image_path)) {
                        File::delete($image_path);
                    }
                }
                //Regular image
                $image = $request->file('userImage');
                $filename = $user->id . '_' . time() . '.' . $image->getClientOriginalExtension();
                $location = public_path('images/user/' . $filename);

                 //-->Circling
                 $image = Image::make($image)->resize(120,120);
                 $width = $image->width();
                 $height = $image->height();
                 $mask = Image::canvas($width, $height);
 
                 // draw a circle
                 $mask->circle($width, $width/2, $height/2, function ($draw) {
                     $draw->background('#000000');
                 });
 
                 $image->mask($mask, false);
 
                 Image::make($image)->save($location);
                 $user->image = $filename;

                //Image::make($image)->resize(120, 120)->save($location);
                //$user->image = $filename;
            }

            $user->save();

            return redirect()->route('users.show', $id)->with('success', 'User has been updated');
        } else {
            return view('pages.notAuth');
            //return("FAILED");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
