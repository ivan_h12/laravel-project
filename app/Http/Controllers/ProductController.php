<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Product;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function detail(){
        return view('pages.productDetail');
    }

    protected function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('pages.productDetail',compact('product'));
    }
    function index(){
        if(Auth::check())
        {
            $products = Product::all();
            $total = Product::all()->count();
            return view('pages.products', compact('products', 'total'));
        }
        else
        {
            return view('pages.notAuth');
        }
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('pages.product', compact('product'));
    }

    protected function create()
    {
        return view('pages.productForm');
    }

    public function store(Request $request)
    {
        if(Auth::check() && Auth::user()->role_id == 1){
            $this->validate($request, [
                'nameProduct' => 'required',
                'priceProduct' => 'required',
                'descriptionProduct' => 'required'
            ]);

            $product = new Product;
            $product->name = $request->nameProduct;
            $product->price = $request->priceProduct;
            $product->description = $request->descriptionProduct;
            $product->save();
            return redirect()->route('products.index')->with('success', 'Product has been added');
        }else{
            return view('pages.notAuth');
        }
  
        
    }
	
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        if(Auth::check() && Auth::user()->role_id == 1) {
            $product->name = $request->nameProduct;
            $product->price = $request->priceProduct;
            $product->description = $request->descriptionProduct;

            $product->save();

            return redirect()->route('products.index')->with('success', 'Product has been updated');
        } else {
            return view('pages.notAuth');
            //return("FAILED");
        }
    }
	
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        
        if(Auth::check() && Auth::user()->role_id == 1) {
            $product->delete();

            return redirect()->route('products.index')->with('success', 'Product has been deleted');
        } else {
            return view('pages.notAuth');
        }
    }
}
